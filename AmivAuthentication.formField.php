<?php

use HTMLFormField;
use MediaWiki\MediaWikiServices;
use OOUI\HtmlSnippet;
use OOUI\Widget;

/**
 * Renders a AMIV Login Button
 */
class AmivAuthenticationFormField extends HTMLFormField {
	/**
	 * @inheritDoc
	 */
	public function __construct( $info ) {
		$info['nodata'] = true;
		parent::__construct( $info );

        $this->redirectUri = $info['redirect-uri'];
	}

	/**
	 * @inheritDoc
	 */
	public function getInputHTML( $value ) {
		$assets = MediaWikiServices::getInstance()->getMainConfig()->get( 'ExtensionAssetsPath' );
		$badgeUrl = htmlspecialchars( $assets . '/PasswordlessLogin/ui/google-play-badge.png' );

        return '<a class="mw-ui-button mw-ui-progressive dataporten-button" href="' . $this->redirectUri . '">Login with AMIV SSO</a>';
	}

	/**
	 * @inheritDoc
	 */
	public function getInputOOUI( $value ) {
		return new Widget( [
			'content' => new HtmlSnippet( $this->getInputHTML( $value ) ),
		] );
	}

	/**
	 * @inheritDoc
	 */
	protected function needsLabel() {
		return false;
	}

	private function getSource() {
		if ( $this->source ) {
			return $this->source;
		}

		return $this->redirectUri;
	}
}
